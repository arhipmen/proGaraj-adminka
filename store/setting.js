import GenericService from "@/services/generic.service";

const name = "setting"
const url = "setting"

const genericService = new GenericService({name, url})

export const state = () => ({
  item: {},
  list: [],
  itemError: null,
})

export const actions = {
  async fetchAll({ commit }) {
    try {
      const items = await genericService.fetchAll();
      commit("fetchItemsSuccess", items);
    } catch (err) {
      commit("fetchItemsFail", {
        errType: `${name} fetchAll failed`,
        err,
      });
    }
  },
  async create({ commit }, payload) {
    try {
      console.log(payload);
      const item = await genericService.create(payload);
      commit("createItemSuccess", item);
    } catch (err) {
      commit("createItemFail", {
        errType: `${name} create failed`,
        err,
      });
      throw err;
    }
  },
  async update({ commit }, { payload, id }) {
    try {
      const item = await genericService.update(id, payload);
      commit("updateItemSuccess", item);
    } catch (err) {
      commit("updateItemFail", {
        errType: `${name} update failed`,
        err,
      });
    }
  },
  async delete({ commit }, id) {
    try {
      const item = await genericService.delete(id);
      commit("deleteItemSuccess", item);
    } catch (err) {
      commit("deleteItemFail", {
        errType: `${name} delete failed`,
        err,
      });
    }
  },
}

export const mutations = {
  fetchItemsSuccess(state, items) {
    state.item = items[0]
    state.list = items;
  },
  fetchItemsFail(state, err) {
    state.itemError = err;
  },

  createItemSuccess(state, item) {
    state.list.push(item)
  },
  createItemFail(state, err) {
    state.itemError = err;
  },

  updateItemSuccess(state, item) {
    state.item[item.name] = item.value;
  },
  updateItemFail(state, err) {
    state.itemError = err;
  },

  deleteItemSuccess() {},
  deleteItemFail(state, err) {
    state.itemError = err;
  },
}
export const getters = {
  item: (state) => state.item,
  list: (state) => state.list,
  itemError: (state) => state.itemError,
}