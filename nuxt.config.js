export default {
  server: {
    port: process.env.PORT,
    hos: process.env.HOST,
  },
  head: {
    title: 'Mevn Dashboard',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  css: [],

  plugins: [
    '~/plugins/axiosport.js',
    '~/plugins/formGenerator.js',
    '~/plugins/vee-validate.js',
  ],
  components: true,
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://auth.nuxtjs.org
    '@nuxtjs/auth-next',
    'cookie-universal-nuxt',
  ],
  tailwindcss: {
    jit: true,
    exposeConfig: true
  },
  buildModules: ['@nuxtjs/tailwindcss'],
  axios: {
    baseURL: process.env.NUXT_APP_BASE_URL,
  },
  auth: {
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'accessToken',
          maxAge: 1800,
          type: 'Bearer'
        },
        refreshToken: {
          property: 'refreshToken',
          data: 'refreshToken',
          maxAge: 60 * 60 * 24 * 30,
        },
        endpoints: {
          user: false,
          refresh: {
            url: '/auth/refresh',
            method: 'post',
          },
          login: {
            url: '/auth/login',
            method: 'post',
            propertyName: 'accessToken',
          },
          logout: {
            url: '/auth/logout',
            method: 'post',
          },
        },
      },
    },
  },
  build: {
    transpile: ['vee-validate/dist/rules']
  },
}
