import crudStore from './generic'

export default {
  ...crudStore({
    url: 'setting',
    name: 'setting',
  }),
}
